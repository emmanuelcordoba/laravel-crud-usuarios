<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">CRUD Usuarios</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item {{ Route::currentRouteName() == 'users.index' ? 'active' : ''}}">
        <a class="nav-link" href="{{ route('users.index') }}">Inicio</a>
      </li>
      <li class="nav-item {{ Route::currentRouteName() == 'users.create' ? 'active' : ''}}">
        <a class="nav-link" href="{{ route('users.create') }}">Nuevo Usuario</a>
      </li>      
    </ul>
    <form class="form-inline my-2 my-lg-0" action="/users" method="GET">
      <input id="buscar" name="buscar" class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Buscar" maxlength="60" >
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    </form>
  </div>
</nav>