@extends('template.main')

@section('content')
    <div class="row">
        <div class="col">
            <h2 class="text-center">{{ $buscar == null ? 'Listado de Usuario' : 'Resultados de la búsqueda' }}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Correo electrónico</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $user->nombre }}</td>
                            <td>{{ $user->apellido }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->telefono }}</td>
                            <td>{{ $user->edad }}</td>
                            <td>
                                <a href="{{ route('users.edit', $user->id) }}" title="Editar Usuario" class="btn btn-warning btn-sm">Editar</a>
                                <button type="button" title="Eliminiar Usuario" class="btn btn-danger btn-sm delete" data-toggle="modal" data-target="#borrarUserModal" data-id="{{ $user->id }}">Borrar</button>
                            </td>
                        </tr>
                    @endforeach
                    @if($users->count() == 0)
                        <tr>
                            <td colspan="7" class="text-center">{{ $buscar == null ? 'Sin usuarios registrados' : 'Sin resultados' }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            {!! $users->links() !!}
        </div>
    </div>

    <div class="modal fade" id="borrarUserModal" tabindex="-1" aria-labelledby="borrarUserModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="borrarUserModal">¿Esta seguro que desea borrar el usuario?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="delete-modal" method="POST" action="{{ route('users.destroy','') }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Borrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
